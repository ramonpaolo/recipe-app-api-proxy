FROM nginx:1.21-alpine

LABEL maintainer "Ramon Paolo Maran"

COPY ./nginx.conf /etc/nginx/conf.d/nginx.conf

COPY ./uwsgi_params /etc/nginx/conf.d/uwsgi_params

ENV LISTEN_PORT 8000
ENV APP_HOST app
ENV APP_PORT 9000

RUN mkdir -p /vol/static
RUN chmod 775 /vol/static
RUN touch /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# CMD [ "nginx", "-g", "daemon", "off;" ]

CMD [ "/entrypoint.sh" ]